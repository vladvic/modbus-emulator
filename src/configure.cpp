/***************************************************
 * configure.cpp
 * Created on Tue, 30 Jul 2019 20:59:40 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <stdexcept>
#include <iostream>
#ifdef __WIN32__
typedef unsigned long ulong;
#endif
#include <pjson.h>
#include "strutil.h"
#include "configure.h"

template<typename Callable>
void iterate_object(const pjson::value_variant &data, const Callable &callback) {
  for(int i = 0; i < data.size(); i ++) {
    std::string name = data.get_key_name_at_index(i);
    const pjson::value_variant &value = data.get_value_at_index(i);
    callback(name, value);
  }
}

class set_output_coil {
  modbus::bus *b;
  int slaveId;

public:
  set_output_coil(const set_output_coil &soc) = default;
  set_output_coil(modbus::bus *bus, int slave) : b(bus), slaveId(slave) { }
  void operator()(std::string const &key, const pjson::value_variant &val) const {
    auto &device = b->get_device(slaveId);
    device.set_output_coil(str::toLong(key), val.as_bool());
  }
};

class set_input_coil {
  modbus::bus *b;
  int slaveId;

public:
  set_input_coil(const set_input_coil &sic) = default;
  set_input_coil(modbus::bus *bus, int slave) : b(bus), slaveId(slave) { }
  void operator()(std::string const &key, const pjson::value_variant &val) const {
    auto &device = b->get_device(slaveId);
    device.set_input_coil(str::toLong(key), val.as_bool());
  }
};

class set_output_register {
  modbus::bus *b;
  int slaveId;

public:
  set_output_register(const set_output_register &sor) = default;
  set_output_register(modbus::bus *bus, int slave) : b(bus), slaveId(slave) { }
  void operator()(std::string const &key, const pjson::value_variant &val) const {
    auto &device = b->get_device(slaveId);
    device.set_output_register(str::toLong(key), val.as_int32());
  }
};

class set_input_register {
  modbus::bus *b;
  int slaveId;

public:
  set_input_register(const set_input_register &sir) = default;
  set_input_register(modbus::bus *bus, int slave) : b(bus), slaveId(slave) { }
  void operator()(std::string const &key, const pjson::value_variant &val) const {
    auto &device = b->get_device(slaveId);
    device.set_input_register(str::toLong(key), val.as_int32());
  }
};

modbus::bus *configure_slave(std::string const &json) {
  std::vector<char> data(json.begin(), json.end());

  pjson::document doc;
  doc.deserialize_in_place(data.data());

  if(!doc.has_key("type")) {
    throw std::runtime_error("Configuration has no modbus type");
  }

  if(!doc.has_key("address")) {
    throw std::runtime_error("Configuration has no modbus address");
  }

  if(!doc.has_key("devices")) {
    throw std::runtime_error("Configuration has no devices");
  }

  std::string type = doc["type"].as_string();
  std::string address = doc["address"].as_string();

  auto &devices = doc["devices"];

  if(!devices.is_array()) {
    throw std::runtime_error("Devices should be array");
  }

  std::cout << "Creating modbus as " << type << ", " << address << std::endl;
  modbus::bus *b = new modbus::slave(type, address);

  for(int i = 0; i < devices.size(); i ++) {
    auto &device = devices[i];
    int slaveId = device["slave-id"].as_int32();
    b->add_device(slaveId);
    iterate_object(device["coils"], set_output_coil(b, slaveId));
    iterate_object(device["input-coils"], set_input_coil(b, slaveId));
    iterate_object(device["registers"], set_output_register(b, slaveId));
    iterate_object(device["input-registers"], set_input_register(b, slaveId));
  }

  return b;
}

modbus::bus *configure_master(std::string const &json) {
  std::vector<char> data(json.begin(), json.end());
  long poll_interval = 10000;

  pjson::document doc;
  doc.deserialize_in_place(data.data());

  if(!doc.has_key("type")) {
    throw std::runtime_error("Configuration has no modbus type");
  }

  if(!doc.has_key("address")) {
    throw std::runtime_error("Configuration has no modbus address");
  }

  if(doc.has_key("poll-interval")) {
    poll_interval = doc["poll-interval"].as_int32();
  }

  if(!doc.has_key("devices")) {
    throw std::runtime_error("Configuration has no devices");
  }

  std::string type = doc["type"].as_string();
  std::string address = doc["address"].as_string();

  auto &devices = doc["devices"];

  if(!devices.is_array()) {
    throw std::runtime_error("Devices should be array");
  }

  std::cout << "Creating modbus as " << type << ", " << address << std::endl;
  modbus::bus *b = new modbus::master(type, address, poll_interval);

  for(int i = 0; i < devices.size(); i ++) {
    auto &device = devices[i];
    int slaveId = device["slave-id"].as_int32();
    b->add_device(slaveId);
    std::cout << "Creating device " << slaveId << std::endl;
    iterate_object(device["coils"], set_output_coil(b, slaveId));
    iterate_object(device["input-coils"], set_input_coil(b, slaveId));
    iterate_object(device["registers"], set_output_register(b, slaveId));
    iterate_object(device["input-registers"], set_input_register(b, slaveId));
  }

  return b;
}
