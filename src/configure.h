/***************************************************
 * configure.h
 * Created on Tue, 30 Jul 2019 20:58:48 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/
#pragma once

#include "bus.h"

modbus::bus *configure_slave(std::string const &json);
modbus::bus *configure_master(std::string const &json);

