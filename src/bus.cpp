/***************************************************
 * bus.cpp
 * Created on Tue, 30 Jul 2019 16:10:38 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <iostream>
#include <stdexcept>
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#ifdef __WIN32__
#include <winsock2.h>
#include <time.h>
#else
#include <sys/select.h>
#endif
#include "bus.h"
#include "strutil.h"

namespace modbus {

exception::exception(int error_number)
  : std::runtime_error(modbus_strerror(error_number))
  , _errno(error_number)
{ }

int exception::error_number() const {
  return _errno;
}

bus::bus(std::string const &type, std::string const &address)
  : _handler(NULL)
  , _sock(-1)
  , _last_sock(-1)
{
  parse_address(type, address);

  if (_type == MODBUS_TCP_PASSIVE || _type == MODBUS_TCP_ACTIVE) {
    int port = MODBUS_TCP_DEFAULT_PORT;
    if (_parameters.size() > 0) {
      port = str::toLong(_parameters[0]);
    }

    _modbus = modbus_new_tcp(_address.c_str(), port);
    _query.resize(MODBUS_TCP_MAX_ADU_LENGTH);
  }
  else {
    if (_parameters.size() < 4) {
      std::string error;
      error = error + "Modbus: " + type + "[" + address + "]: not enough parameters";
      throw std::runtime_error(error.c_str());
    }
    int baudrate = str::toLong(_parameters[0]);
    char parity = _parameters[1][0];
    int bits = str::toLong(_parameters[2]);
    int stopBits = str::toLong(_parameters[3]);
    int serialMode = -1;

    if (_parameters.size() > 4) {
      if (_parameters[4] == "RS232") {
        serialMode = MODBUS_RTU_RS232;
      }
      else if (_parameters[4] == "RS485") {
        serialMode = MODBUS_RTU_RS485;
      }
    }

    switch(_type) {
    case MODBUS_RTU:
      std::cout << "Opening modbus device: " << _address << ":" << baudrate << "," << parity 
                << "," << bits << "," << stopBits << std::endl;
      _modbus = modbus_new_rtu(_address.c_str(), baudrate, parity, bits, stopBits);
      _query.resize(MODBUS_RTU_MAX_ADU_LENGTH);
      if (serialMode >= 0) {
        modbus_rtu_set_serial_mode(_modbus, serialMode);
      }
      break;
    case MODBUS_ASCII:
      std::cout << "Opening modbus device: " << _address << ":" << baudrate << "," 
                << parity << "," << bits << "," << stopBits << " in ASCII mode" << std::endl;
      _modbus = modbus_new_ascii(_address.c_str(), baudrate, parity, bits, stopBits);
      _query.resize(MODBUS_ASCII_MAX_ADU_LENGTH);
      if (serialMode >= 0) {
        modbus_ascii_set_serial_mode(_modbus, serialMode);
      }
      break;
    default:
      break;
    }
  }

	modbus_set_debug(_modbus, 0);

  if (_type != MODBUS_TCP_PASSIVE) {
    modbus_set_error_recovery(_modbus, (modbus_error_recovery_mode)(MODBUS_ERROR_RECOVERY_PROTOCOL));
    int rc = modbus_connect(_modbus);
    if (rc < 0) {
      modbus_free(_modbus);
      std::string error;
      error = error + "Modbus: " + type + "[" + address + "]: failed to open device";
      throw std::runtime_error(error.c_str());
    }
  }
  else {
    modbus_set_error_recovery(_modbus, (modbus_error_recovery_mode)(MODBUS_ERROR_RECOVERY_PROTOCOL));
    _sock = modbus_tcp_listen(_modbus, 5);
    if (_sock < 0) {
      modbus_free(_modbus);
      throw std::runtime_error("Failed to open TCP modbus listening socket");
    }
  }
}

bus::~bus() {
  modbus_free(_modbus);
}

void bus::parse_address(std::string const &type, std::string const &addr) {
  int start = 0;
  bool hasAddress = false;
  std::string value;

  if (type == "tcp-listen") {
    _type = MODBUS_TCP_PASSIVE;
  } else if (type == "tcp") {
    _type = MODBUS_TCP_ACTIVE;
  } else if (type == "rtu") {
    _type = MODBUS_RTU;
  } else if (type == "ascii") {
    _type = MODBUS_ASCII;
  } else {
    std::string error = std::string("Unknown modbus type: ") + type;
    throw std::runtime_error(error.c_str());
  }

  for(int i = 0; i < addr.size(); i ++) {
    if (addr[i] == ':') {
      value = addr.substr(start, i - start);
      start = i + 1;
      _address = value;
      hasAddress = true;
    }
    else if (addr[i] == ',') {
      value = addr.substr(start, i - start);
      start = i + 1;
      _parameters.emplace_back(std::move(value));
    }
    else if (i == addr.size() - 1) {
      value = addr.substr(start, i + 1 - start);
      if (!hasAddress) {
        _address = value;
      }
      else {
        _parameters.emplace_back(std::move(value));
      }
    }
  }
}

void bus::add_device(int id) {
  _devices.insert(std::make_pair(id, device(id)));
  if(_handler) {
    _handler->device_added(get_device(id));
  }
}

std::map<int, device> &bus::get_devices() {
  return _devices;
}

device &bus::get_device(int id) {
  auto dev = _devices.find(id);

  if (dev == _devices.end()) {
    dev = _devices.insert(std::make_pair(id, device(id))).first;
  }

  return dev->second;
}

int bus::can_read(int sock, int timeout) {
  struct timeval to;
  fd_set rdset;
  FD_ZERO(&rdset);
  FD_SET(sock, &rdset);
  to.tv_sec = timeout / 1000;
  to.tv_usec = (timeout % 1000) * 1000;
  int rc = select(sock + 1, &rdset, NULL, NULL, &to);

  if (rc < 0) {
    return 0;
  }

  if (FD_ISSET(sock, &rdset)) {
    return 1;
  }

  return 0;
}

void bus::set_max_registers(int mr) {
  _values.resize((mr + 1) * 2);
}

void bus::set_bus_handler(bus_handler *h) {
  _handler = h;
}

bool bus::is_connected() {
  int s = modbus_get_socket(_modbus);
  return s >= 0;
}

int bus::process(long timeout) {
  int s = modbus_get_socket(_modbus);

	modbus_set_response_timeout(_modbus, timeout / 1000, (timeout % 1000) * 1000);

  if ((_last_sock >= 0) && (s < 0)) {
    _handler->log(log_level::info, "Connection was closed: #%d", _last_sock);
    if (_handler) {
      _handler->connection_error();
    }
  }

  if (s < 0) {
    if (_type == MODBUS_TCP_PASSIVE) {
      if (can_read(_sock, timeout)) {
        s = modbus_tcp_accept(_modbus, &_sock);
        if (s >= 0) {
          if (_handler) {
            _handler->log(log_level::info, "Connection accepted: #%d", s);
          }
        }
      }
    }
    else {
      if (modbus_connect(_modbus) == 0) {
        s = modbus_get_socket(_modbus);
        if (_handler) {
          _handler->log(log_level::info, "Connection restored: #%d", s);
        }
      }
    }
  }

  _last_sock = s;
  return s;
}


device::device(int id)
  : _id(id)
{ }

device::~device() {
}

void device::write_coil(long addr, uint8_t data) {
  _coils_write_cache[addr] = data;
}

bool device::has_write_coil(long addr) {
  return _coils_write_cache.find(addr) != _coils_write_cache.end();
}

void device::clear_write_coils() {
  _coils_write_cache.clear();
}

void device::write_register(long addr, int16_t data, int16_t mask) {
  int16_t new_value = data;
  int16_t new_mask = mask;

  if (_registers_write_cache.find(addr) != _registers_write_cache.end()) {
    new_value = (_registers_write_cache[addr] & (~mask)) | (data & mask);
    new_mask = _registers_write_cache_mask[addr] | mask;
  }

  _registers_write_cache[addr] = new_value;
  _registers_write_cache_mask[addr] = new_mask;
}

bool device::has_write_register(long addr) {
  return _registers_write_cache.find(addr) != _registers_write_cache.end();
}

void device::clear_write_registers() {
  _registers_write_cache.clear();
}

long device::get_input_coil(long addr) {
  // TODO: Check overflow
  if (_input_coils.find(addr) != _input_coils.end()) {
    return _input_coils[addr];
  }
  return 0;
}
long device::get_output_coil(long addr) {
  //addr -= 10000;
  // TODO: Check overflow
  if (_coils.find(addr) != _coils.end()) {
    return _coils[addr];
  }
  return 0;
}
long device::get_input_register(long addr) {
  //addr -= 30000;
  // TODO: Check overflow
  if (_input_registers.find(addr) != _input_registers.end()) {
    return _input_registers[addr];
  }
  return 0;
}
long device::get_output_register(long addr) {
  //addr -= 20000;
  // TODO: Check overflow
  if (_registers.find(addr) != _registers.end()) {
    return _registers[addr];
  }
  return 0;
}

void device::set_input_coil(long addr, int data) {
  _input_coils[addr] = data ? 1 : 0;
}
void device::set_output_coil(long addr, int data) {
  _coils[addr] = data ? 1 : 0;
}
void device::set_input_register(long addr, long data) {
  _input_registers[addr] = data;
}
void device::set_output_register(long addr, long data) {
  _registers[addr] = data;
}

void device::set_output_coils(uint8_t *data, long addr, int num) {
  for(int i = addr; i < (addr + num); i ++) {
    if (data[i] != 0) {
      _coils[i] = data[i];
    } else {
      if (_coils.find(i) != _coils.end()) {
        _coils[i] = data[i];
      }
    }
  }
}
void device::set_output_registers(int16_t *data, long addr, int num) {
  for(int i = addr; i < (addr + num); i ++) {
    if (data[i] != 0) {
      _registers[i] = data[i];
    } else {
      if (_registers.find(i) != _registers.end()) {
        _registers[i] = data[i];
      }
    }
  }
}

void device::get_input_coils(uint8_t *data, long addr, int num) {
  for(int i = addr; i < (addr + num); i ++) {
    if (_input_coils.find(i) != _input_coils.end()) {
      data[i] = _input_coils[i];
    }
  }
}
void device::get_output_coils(uint8_t *data, long addr, int num) {
  for(int i = addr; i < (addr + num); i ++) {
    if (_coils.find(i) != _coils.end()) {
      data[i] = _coils[i];
    }
  }
}
void device::get_input_registers(int16_t *data, long addr, int num) {
  for(int i = addr; i < (addr + num); i ++) {
    if (_input_registers.find(i) != _input_registers.end()) {
      data[i] = _input_registers[i];
    }
  }
}
void device::get_output_registers(int16_t *data, long addr, int num) {
  for(int i = addr; i < (addr + num); i ++) {
    if (_registers.find(i) != _registers.end()) {
      data[i] = _registers[i];
    }
  }
}

void bus_handler::registers_written(device &dev, int16_t *buffer, int addr, int num) {
  dev.set_output_registers(buffer, addr, num);
}

void bus_handler::coils_written(device &dev, uint8_t *buffer, int addr, int num) {
  dev.set_output_coils(buffer, addr, num);
}

void bus_handler::log(log_level severity, const char *format, ...) {
  va_list args;
  va_start(args, format);
  char fmt2[strlen(format) + 5];
  sprintf(fmt2, "%s\n", format);
  vprintf(fmt2, args);
  va_end(args);
}


slave::slave(std::string const &type, std::string const &address) 
  : bus(type, address)
{
  memset(&_mapping, 0, sizeof(_mapping));
}

int slave::process(long timeout) {
  int s;

  if ((s = bus::process(timeout)) < 0) {
    return -1;
  }

  long byte_timeout = timeout / 10;
	modbus_set_response_timeout(_modbus, timeout / 1000, (timeout % 1000) * 1000);
  modbus_set_byte_timeout(_modbus, byte_timeout / 1000, (byte_timeout % 1000) * 1000);
  int rc = modbus_receive(_modbus, _query.data());

  if (rc == -1) {
    if (errno == ECONNRESET) {
      if(_type == MODBUS_TCP_PASSIVE || _type == MODBUS_TCP_ACTIVE) {
        if (_handler) {
          _handler->log(log_level::warning, "Connection closed: #%d", s);
        }
        modbus_close(_modbus);
      }
    }
    return 0;
  }

  int hlen = modbus_get_header_length(_modbus);

  int slaveId = _query[hlen - 1];
  int function = _query[hlen];
  uint16_t addr = (_query[hlen + 1] << 8) + _query[hlen + 2];
  int nb = (_query[hlen + 3] << 8) + _query[hlen + 4];

  if (_devices.find(slaveId) != _devices.end()) {
    modbus::device &dev = this->get_device(slaveId);
    switch(function) {
    case MODBUS_FC_READ_COILS:
      dev.get_output_coils(_values.data(), addr, nb);
    case MODBUS_FC_WRITE_SINGLE_COIL:
    case MODBUS_FC_WRITE_MULTIPLE_COILS:
      _mapping.nb_bits = _values.size() / 2;
      _mapping.tab_bits = _values.data();
      break;

    case MODBUS_FC_WRITE_AND_READ_REGISTERS:
    case MODBUS_FC_READ_HOLDING_REGISTERS:
      dev.get_output_registers((int16_t*)_values.data(), addr, nb);
    case MODBUS_FC_WRITE_SINGLE_REGISTER:
    case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
      _mapping.nb_registers = _values.size();
      _mapping.tab_registers = (uint16_t*)_values.data();
      break;

    case MODBUS_FC_READ_DISCRETE_INPUTS:
      dev.get_input_coils(_values.data(), addr, nb);
      _mapping.nb_input_bits = _values.size() / 2;
      _mapping.tab_input_bits = _values.data();
      break;

    case MODBUS_FC_READ_INPUT_REGISTERS:
      dev.get_input_registers((int16_t*)_values.data(), addr, nb);
      _mapping.nb_input_registers = _values.size() / 2;
      _mapping.tab_input_registers = (uint16_t*)_values.data();
    }

    rc = modbus_reply(_modbus, _query.data(), rc, &_mapping);

    switch(function) {
    case MODBUS_FC_WRITE_SINGLE_COIL:
      nb = 1;
    case MODBUS_FC_WRITE_MULTIPLE_COILS:
      if (_handler) {
        _handler->coils_written(dev, (uint8_t*)_values.data(), addr, nb);
      }
    case MODBUS_FC_READ_COILS:
      _mapping.nb_bits = 0;
      _mapping.tab_bits = NULL;
      break;

    case MODBUS_FC_WRITE_AND_READ_REGISTERS:
      addr = (_query[hlen + 5] << 8) + _query[hlen + 6];
      nb = (_query[hlen + 7] << 8) + _query[hlen + 8];
    case MODBUS_FC_WRITE_SINGLE_REGISTER:
      if (function == MODBUS_FC_WRITE_SINGLE_REGISTER) {
        nb = 1;
      }
    case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
      if (_handler) {
        _handler->registers_written(dev, (int16_t*)_values.data(), addr, nb);
      }
    case MODBUS_FC_READ_HOLDING_REGISTERS:
      _mapping.nb_registers = 0;
      _mapping.tab_registers = NULL;
      break;

    case MODBUS_FC_READ_DISCRETE_INPUTS:
      _mapping.nb_input_bits = 0;
      _mapping.tab_input_bits = NULL;
      break;

    case MODBUS_FC_READ_INPUT_REGISTERS:
      _mapping.nb_input_registers = 0;
      _mapping.tab_input_registers = NULL;
    }
  }
  else {
    if (_handler) {
      _handler->log(log_level::warning, "No slave with ID %d", slaveId);
    }
  }
  return 0;
}


master::master(std::string const &type, std::string const &address, unsigned long wait)
  : bus(type, address)
  , _processed(_devices.end())
  , _process_type(0)
  , _last_access(0)
  , _wait_time(wait)
{ }

void master::wait_time(long time_us) {
	struct timespec now;
  unsigned long now_ts, diff;

  do {
    clock_gettime(CLOCK_MONOTONIC, &now);
    now_ts = now.tv_sec * 1000000 + now.tv_nsec / 1000;
    diff = (now_ts - _last_access);
    if(diff < time_us) {
      long wait = (time_us - diff);
      if ((time_us - diff) > 50) {
        wait /= 2;
      }
      usleep(wait);
    }
  } while(diff < time_us);

  _last_access = now_ts;
}

bool master::write_coils(device &dev) {
  uint8_t *data = _values.data();
  int first_addr = -1, last_addr = -1;
  int nb = 0;

  // Select cached write requests
  for(auto coil = dev._coils_write_cache.begin(); coil != dev._coils_write_cache.end(); ++ coil) {
    int current = coil->first;

    if (last_addr < 0) {
      last_addr = current;
      first_addr = current;
    }

    if (coil->first > (last_addr + 1)) {
      break;
    }

    ++ nb;
    data[current] = coil->second;
    last_addr = current;
  }

  // Try to write
  modbus_set_slave(_modbus, dev._id);
  wait_time(_wait_time);
  int rc = modbus_write_bits(_modbus, first_addr, nb, &data[first_addr]);

  for(int i = 0; i < nb; i ++) {
    dev._coils_write_cache.erase(first_addr + i);
  }

  // If succeeded, delete write requests
  if (rc >= 0) {
    if (_handler) {
      dev.set_output_coils(data, first_addr, nb);
      _handler->coils_written(dev, data, first_addr, nb);
    }
  }
  else {
    if (_handler) {
      _handler->log(log_level::warning, "Error writing coils: %s", modbus_strerror(errno));
      _handler->coils_write_error(dev, data, first_addr, nb);
    }
  }

  return nb != 0;
}

bool master::write_registers(device &dev) {
  uint16_t *data = (uint16_t*)_values.data();
  int first_addr = -1, last_addr = -1;
  int nb = 0;

  // Select cached write requests
  for(auto reg = dev._registers_write_cache.begin(); reg != dev._registers_write_cache.end(); ++ reg) {
    int current = reg->first;
    int16_t mask = dev._registers_write_cache_mask[current];
    int16_t value = dev.get_output_register(current) & (~mask);

    if (last_addr < 0) {
      last_addr = current;
      first_addr = current;
    }

    if (reg->first > (last_addr + 1)) {
      break;
    }

    ++ nb;
    data[current] = value | (reg->second & mask);
    last_addr = current;
  }

  // Try to write
  modbus_set_slave(_modbus, dev._id);
  wait_time(_wait_time);
  int rc = modbus_write_registers(_modbus, first_addr, nb, &data[first_addr]);

  for(int i = 0; i < nb; i ++) {
    dev._registers_write_cache.erase(first_addr + i);
    dev._registers_write_cache_mask.erase(first_addr + i);
  }

  // If succeeded, delete write requests
  if (rc >= 0) {
    if (_handler) {
      dev.set_output_registers((int16_t*)data, first_addr, nb);
      _handler->registers_written(dev, (int16_t*)data, first_addr, nb);
    }
  }
  else {
    if (_handler) {
      _handler->log(log_level::warning, "Error writing registers: %s", modbus_strerror(errno));
      _handler->registers_write_error(dev, (int16_t*)data, first_addr, nb);
    }
  }

  return nb != 0;
}

bool master::flush_write() {
  std::map<int, device>::iterator current = _devices.begin();

  while(current != _devices.end()) {
    if (!current->second._coils_write_cache.empty()) {
      return write_coils(current->second);
    }

    if (!current->second._registers_write_cache.empty()) {
      return write_registers(current->second);
    }
    ++ current;
  }

  return false;
}

void master::update_input_coils(device &dev, uint8_t *data, int first_addr, int nb) {
  if (nb == 0) {
    return;
  }
  wait_time(_wait_time);
  int rc = modbus_read_input_bits(_modbus, first_addr, nb, &data[first_addr]);
  if (rc >= 0) {
    if (_handler) {
      _handler->input_coils_changed(dev, data, first_addr, nb);
    }
    for(int i = 0; i < nb; i ++) {
      dev._input_coils[first_addr + i] = data[first_addr + i];
    }
  }
  else {
    if (_handler) {
      _handler->log(log_level::warning, "Error reading input coils dev=%d,addr=%d,nb=%d: %s", dev.get_id(), first_addr, nb, modbus_strerror(errno));
      _handler->input_coils_read_error(dev, data, first_addr, nb);
    }
    throw exception(errno);
  }
}

void master::update_coils(device &dev, uint8_t *data, int first_addr, int nb) {
  if (nb == 0) {
    return;
  }
  wait_time(_wait_time);
  int rc = modbus_read_bits(_modbus, first_addr, nb, &data[first_addr]);
  if (rc >= 0) {
    if (_handler) {
      _handler->coils_changed(dev, data, first_addr, nb);
    }
    for(int i = 0; i < nb; i ++) {
      dev._coils[first_addr + i] = data[first_addr + i];
    }
  }
  else {
    if (_handler) {
      _handler->log(log_level::warning, "Error reading coils dev=%d,addr=%d,nb=%d: %s", dev.get_id(), first_addr, nb, modbus_strerror(errno));
      _handler->coils_read_error(dev, data, first_addr, nb);
    }
    throw exception(errno);
  }
}

void master::update_input_registers(device &dev, uint16_t *data, int first_addr, int nb) {
  if (nb == 0) {
    return;
  }
  wait_time(_wait_time);
  int rc = modbus_read_input_registers(_modbus, first_addr, nb, &data[first_addr]);
  if (rc >= 0) {
    if (_handler) {
      _handler->input_registers_changed(dev, (int16_t*)data, first_addr, nb);
    }
    for(int i = 0; i < nb; i ++) {
      dev._input_registers[first_addr + i] = data[first_addr + i];
    }
  }
  else {
    if (_handler) {
      _handler->log(log_level::warning, "Error reading input registers dev=%d,addr=%d,nb=%d: %s", dev.get_id(), first_addr, nb, modbus_strerror(errno));
      _handler->input_registers_read_error(dev, (int16_t*)data, first_addr, nb);
    }
    throw exception(errno);
  }
}

void master::update_registers(device &dev, uint16_t *data, int first_addr, int nb) {
  if (nb == 0) {
    return;
  }
  wait_time(_wait_time);
  int rc = modbus_read_registers(_modbus, first_addr, nb, &data[first_addr]);
  if (rc >= 0) {
    if (_handler) {
      _handler->registers_changed(dev, (int16_t*)data, first_addr, nb);
    }
    for(int i = 0; i < nb; i ++) {
      dev._registers[first_addr + i] = data[first_addr + i];
    }
  }
  else {
    if (_handler) {
      _handler->log(log_level::warning, "Error while reading registers dev=%d,addr=%d,nb=%d: %s", dev.get_id(), first_addr, nb, modbus_strerror(errno));
      _handler->registers_read_error(dev, (int16_t*)data, first_addr, nb);
    }
    throw exception(errno);
  }
}

bool master::poll_input_coils(device &dev) {
  uint8_t *data = (uint8_t*)_values.data();
  int first_addr = -1, last_addr = -1, nb = 0;
  bool result = false;

  if (dev._input_coils.empty()) {
    return false;
  }

  // Select cached write requests
  for(auto coil = dev._input_coils.begin(); coil != dev._input_coils.end(); ++ coil) {
    int current = coil->first;

    if (first_addr < 0) {
      last_addr = current;
      first_addr = current;
    }

    if (current > (last_addr + 1)) {
      try {
        update_input_coils(dev, data, first_addr, nb);
      }
      catch(modbus::exception &e) { }
      first_addr = current;
      nb = 0;
      result = true;
    }

    last_addr = current;
    ++ nb;
  }

  if (nb) {
    update_input_coils(dev, data, first_addr, nb);
    result = true;
  }

  return result;
}

bool master::poll_coils(device &dev) {
  uint8_t *data = (uint8_t*)_values.data();
  int first_addr = -1, last_addr = -1, nb = 0;
  bool result = false;

  if (dev._coils.empty()) {
    return false;
  }

  // Select cached write requests
  for(auto coil = dev._coils.begin(); coil != dev._coils.end(); ++ coil) {
    int current = coil->first;

    if (last_addr < 0) {
      last_addr = current;
      first_addr = current;
    }

    if (current > (last_addr + 1)) {
      try {
        update_coils(dev, data, first_addr, nb);
      }
      catch(modbus::exception &e) { }
      first_addr = current;
      nb = 0;
      result = true;
    }

    last_addr = current;
    ++ nb;
  }

  if (nb) {
    update_coils(dev, data, first_addr, nb);
    result = true;
  }

  return result;
}

bool master::poll_input_registers(device &dev) {
  uint16_t *data = (uint16_t*)_values.data();
  int first_addr = -1, last_addr = -1, nb = 0;
  bool result = false;

  if (dev._input_registers.empty()) {
    return false;
  }

  // Select cached write requests
  for(auto reg = dev._input_registers.begin(); reg != dev._input_registers.end(); ++ reg) {
    int current = reg->first;

    if (last_addr < 0) {
      last_addr = current;
      first_addr = current;
    }

    if (current > (last_addr + 1)) {
      try {
        update_input_registers(dev, data, first_addr, nb);
      }
      catch(modbus::exception &e) { }
      first_addr = current;
      nb = 0;
      result = true;
    }

    last_addr = current;
    ++ nb;
  }

  if (nb) {
    update_input_registers(dev, data, first_addr, nb);
    result = true;
  }

  return result;
}

bool master::poll_registers(device &dev) {
  uint16_t *data = (uint16_t*)_values.data();
  int first_addr = -1, last_addr = -1, nb = 0;
  bool result = false;

  if (dev._registers.empty()) {
    return false;
  }

  // Select cached write requests
  for(auto reg = dev._registers.begin(); reg != dev._registers.end(); ++ reg) {
    int current = reg->first;

    if (last_addr < 0) {
      last_addr = current;
      first_addr = current;
    }

    if (current > (last_addr + 1)) {
      try {
        update_registers(dev, data, first_addr, nb);
      }
      catch(modbus::exception &e) { }
      first_addr = current;
      nb = 0;
      result = true;
    }

    last_addr = current;
    ++ nb;
  }

  if (nb) {
    update_registers(dev, data, first_addr, nb);
    result = true;
  }

  return result;
}

#define TYPE_INPUT_COILS  0
#define TYPE_COILS        1
#define TYPE_INPUT_REGS   2
#define TYPE_REGS         3
int master::process(long timeout) {
  int s;

  if ((s = bus::process(timeout)) < 0) {
    return -1;
  }

  if (_devices.empty()) {
    usleep(timeout * 1000);
    return 0;
  }

  if (_processed != _devices.end()) {
    ++ _processed;
  }

  if (_processed == _devices.end()) {
    _processed = _devices.begin();
  }

  long byte_timeout = timeout / 2;
  modbus_set_byte_timeout(_modbus, byte_timeout / 1000, (byte_timeout % 1000) * 1000);
	modbus_set_response_timeout(_modbus, timeout / 1000, (timeout % 1000) * 1000);

  if (flush_write()) {
    return 0;
  }

  modbus_set_slave(_modbus, _processed->second._id);

  try {
    switch(_process_type) {
    case TYPE_INPUT_COILS:
      ++ _process_type;
      if (poll_input_coils(_processed->second)) {
        break;
      }
    case TYPE_COILS:
      ++ _process_type;
      if (poll_coils(_processed->second)) {
        break;
      }
    case TYPE_INPUT_REGS:
      ++ _process_type;
      if (poll_input_registers(_processed->second)) {
        break;
      }
    case TYPE_REGS:
      ++ _process_type;
      if (poll_registers(_processed->second)) {
        break;
      }
    }
  }
  catch(std::exception &e) {
  }

  if (errno == ECONNRESET) {
    if(_type == MODBUS_TCP_PASSIVE || _type == MODBUS_TCP_ACTIVE) {
      if (_handler) {
        _handler->log(log_level::warning, "Connection closed: #%d", s);
      }
      modbus_close(_modbus);
      return -1;
    }
  }
  else if (errno == EPIPE) {
    modbus_close(_modbus);
    return -1;
  }

  if (_process_type > TYPE_REGS) {
    _process_type = 0;
  }

  return 0;
}


}

