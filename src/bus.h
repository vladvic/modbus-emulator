/***************************************************
 * bus.h
 * Created on Tue, 30 Jul 2019 16:07:33 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/
#pragma once

#include <string>
#include <vector>
#include <stdexcept>
#include <map>
#include <modbus.h>

namespace modbus {
class bus;
class slave;
class master;

class exception 
  : public std::runtime_error {
public:
  exception(int error_number);
  int error_number() const;

private:
  int _errno;
};

class device {
  friend class bus;
  friend class slave;
  friend class master;
  int _id;

  std::map<int, uint8_t> _coils_write_cache;
  std::map<int, int16_t> _registers_write_cache;
  std::map<int, int16_t> _registers_write_cache_mask;

  std::map<int, uint8_t> _coils;
  std::map<int, uint8_t> _input_coils;
  std::map<int, int16_t> _registers;
  std::map<int, int16_t> _input_registers;

  void get_input_coils(uint8_t *data, long addr, int num);
  void get_output_coils(uint8_t *data, long addr, int num);
  void get_input_registers(int16_t *data, long addr, int num);
  void get_output_registers(int16_t *data, long addr, int num);

public:
  device(const device &dev) = default;
  device(int id);
  ~device();

  void write_coil(long addr, uint8_t data);
  bool has_write_coil(long addr);
  void clear_write_coils();
  void write_register(long addr, int16_t data, int16_t mask = 0xffff);
  bool has_write_register(long addr);
  void clear_write_registers();

  long get_input_coil(long addr);
  long get_output_coil(long addr);
  long get_input_register(long addr);
  long get_output_register(long addr);

  void set_input_coil(long addr, int data);
  void set_output_coil(long addr, int data);
  void set_input_register(long addr, long data);
  void set_output_register(long addr, long data);

  void set_output_coils(uint8_t *data, long addr, int num);
  void set_output_registers(int16_t *data, long addr, int num);

  int get_id() { return _id; }
};

enum class log_level {
  error = 0,
  warning,
  info,
  debug
};

class bus_handler {
public:
  virtual ~bus_handler() { }
  virtual void device_added(device &dev) { }
  virtual void input_coils_changed(device &dev, uint8_t *buffer, int addr, int num) { }
  virtual void registers_changed(device &dev, int16_t *buffer, int addr, int num) { }
  virtual void coils_changed(device &dev, uint8_t *buffer, int addr, int num) { }
  virtual void input_registers_changed(device &dev, int16_t *buffer, int addr, int num) { }

  virtual void registers_write_error(device &dev, int16_t *buffer, int addr, int num) { }
  virtual void coils_write_error(device &dev, uint8_t *buffer, int addr, int num) { }
  virtual void registers_written(device &dev, int16_t *buffer, int addr, int num);
  virtual void coils_written(device &dev, uint8_t *buffer, int addr, int num);

  virtual void input_coils_read_error(device &dev, uint8_t *buffer, int addr, int num) { }
  virtual void registers_read_error(device &dev, int16_t *buffer, int addr, int num) { }
  virtual void coils_read_error(device &dev, uint8_t *buffer, int addr, int num) { }
  virtual void input_registers_read_error(device &dev, int16_t *buffer, int addr, int num) { }

  virtual void connection_error() { }

  virtual void log(log_level severity, const char *format, ...);
};

class bus {
protected:
  modbus_t *_modbus;
  enum {
    MODBUS_TCP_PASSIVE = 0,
    MODBUS_TCP_ACTIVE,
    MODBUS_RTU,
    MODBUS_ASCII
  } _type;
  bus_handler *_handler;
  std::string _address;
  std::vector<std::string> _parameters;
  std::vector<uint8_t> _query;
  std::vector<uint8_t> _values;
  std::map<int, device> _devices;
  int _last_sock;
  int _sock;

  void parse_address(std::string const &type, std::string const &addr);
  int can_read(int sock, int timeout);

public:
  bus(std::string const &type, std::string const &address);
  ~bus();
  void add_device(int id);
  device &get_device(int id);
  std::map<int, device> &get_devices();
  virtual int process(long timeout) = 0;
  void set_max_registers(int mr);
  void set_bus_handler(bus_handler *h);
  bool is_connected();
};

class slave : public bus {
  modbus_mapping_t _mapping;

public:
  slave(std::string const &type, std::string const &address);
  int process(long timeout);
};

class master : public bus {
  std::map<int, device>::iterator _processed;
  int _process_type;
  unsigned long _last_access;
  unsigned long _wait_time;

  void wait_time(long time_us);

  bool write_coils(device &dev);
  bool write_registers(device &dev);

  bool flush_write();

  void update_input_coils(device &dev, uint8_t *data, int first, int nb);
  void update_coils(device &dev, uint8_t *data, int first, int nb);
  void update_input_registers(device &dev, uint16_t *data, int first, int nb);
  void update_registers(device &dev, uint16_t *data, int first, int nb);

  bool poll_input_coils(device &dev);
  bool poll_coils(device &dev);
  bool poll_input_registers(device &dev);
  bool poll_registers(device &dev);

public:
  master(std::string const &type, std::string const &address, unsigned long wait = 10000);
  int process(long timeout);
};

}

