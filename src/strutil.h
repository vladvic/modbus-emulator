/***************************************************
 * strutil.h
 * Created on Tue, 30 Jul 2019 16:24:28 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/
#pragma once

#include <vector>
#include <string>

namespace str {

long toLong(std::string const &);
std::vector<std::string> split(std::string const &, char);

}

