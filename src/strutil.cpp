/***************************************************
 * strutil.cpp
 * Created on Tue, 30 Jul 2019 16:24:52 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <stdlib.h>
#include "strutil.h"

namespace str {

long toLong(std::string const &str) {
  int base = 10;
  const char *num = str.c_str();

  if(*num == 'b') {
    base = 2;
    num ++;
  }
  else if(*num == '0' && (str.size() > 1)) {
    base = 8;
    num ++;
    if(*num == 'x') {
      base = 16;
      num ++;
    }
  }

  return strtol(num, NULL, base);
}

std::vector<std::string> split(std::string const &str, char what) {
  int start = 0, end = 0;
  std::string value;
  std::vector<std::string> result;

  for(int i = 0; i < str.size(); i ++) {
    if(str[i] == what) {
      value = str.substr(start, i - start);
      start = i + 1;
      result.emplace_back(std::move(value));
    }
    else if(i == str.size() - 1) {
      value = str.substr(start, i + 1 - start);
      result.emplace_back(std::move(value));
    }
  }

  return result;
}

}
