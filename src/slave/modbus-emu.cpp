/***************************************************
 * modbus-emu.cpp
 * Created on Tue, 30 Jul 2019 15:47:53 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <getopt.h>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <streambuf>
#include "bus.h"
#include "configure.h"

#ifdef __WIN32__
void socket_library_init() __attribute__((constructor(2000)));

void socket_library_init() {
  WSADATA wsd;
  WSAStartup(MAKEWORD(2, 0), &wsd);
}
#endif

int help(int argc, char **argv) {
  printf("Usage: %s <options>\n", argv[0]);

  printf("--type,-t <type>            modbus type <tcp|tcp-listen|rtu|ascii>\n");
  printf("--address,-a <addr>         modbus device address: <host>:<port>, <serial device>:<speed>,<bits>,<parity>,<stop-bits>\n");
  printf("--config,-c <config>        configuration file\n");
  printf("--help,-h                   output help\n");
  return 0;
}

int main(int argc, char **argv) {
  std::string type, addr;
  modbus::bus_handler handler;
  modbus::bus *b;

  if(argc < 2) {
    help(argc, argv);
    return 0;
  }

  static struct option long_options[] =
  {
    /* These options don’t set a flag.
       We distinguish them by their indices. */
    {"type",    1, 0, 't'},
    {"address", 1, 0, 'a'},
    {"config",  1, 0, 'c'},
    {"help",    0, 0, 'h'},
    {0, 0, 0, 0}
  };
  int i, option_index = 0;

  while(1) {
    int c = getopt_long(argc, argv, "t:a:c:h", long_options, &option_index);

    if(c == -1)
      break;

    switch(c) {
    case 't':
      type = optarg;
      break;
    case 'a':
      addr = optarg;
      break;
    case 'c':
      {
      std::ifstream configFile(optarg);
      std::string config((std::istreambuf_iterator<char>(configFile)),
                          std::istreambuf_iterator<char>());
      try {
        b = configure_slave(config);
      } catch(std::exception &e) {
        std::cout << "Config error: " << e.what() << std::endl;
        return -1;
      }
      }
      break;
    case 'h':
      help(argc, argv);
      return 0;
    }
  }

  if(b == NULL) {
    b = new modbus::slave(type, addr);
    b->add_device(1);
  }

  b->set_max_registers(9999);
  b->set_bus_handler(&handler);

  while(1) {
    try {
      b->process(10000);
    } catch(std::exception &e) {
      std::cout << e.what() << std::endl;
    }
  }

  delete b;
}
