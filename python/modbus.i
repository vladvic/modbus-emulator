%module(directors="1") pymodbus
%{
#define SWIG_FILE_WITH_INIT
#include <iostream>
#include <stdint.h>
#include <vector>
#include "bus.h"
#include "pybushandler.h"

void cleanup() {
}

%}

%init
%{
  PyEval_InitThreads();
  atexit(cleanup);
%}

%exception {
  try {
    $action
  }
  catch (Swig::DirectorMethodException &e) {
    SWIG_exception(SWIG_UnknownError, "Director method exception");
    SWIG_fail;
  }
  catch (std::exception &e) {
    SWIG_exception(SWIG_UnknownError, e.what());
    SWIG_fail;
  }
}

%feature("director:except") {
  if ($error != NULL) {
    PyErr_Print();
    throw Swig::DirectorMethodException();
  }
} 

%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"
%include "stdint.i"
%include "exception.i"

%template (CoilsList) std::vector<bool>;
%template (RegistersList) std::vector<int16_t>;

%extend modbus::device {
  void set_input_coil(long addr, bool data) {
    $self->set_input_coil(addr, (int)data);
  }
  void set_output_coil(long addr, bool data) {
    $self->set_output_coil(addr, (int)data);
  }
  void set_input_register(long addr, double data) {
    $self->set_input_register(addr, (long)data);
  }
  void set_output_register(long addr, double data) {
    $self->set_output_register(addr, (long)data);
  }
}

%feature("director") BusHandler;
%feature("nodirector") BusHandler::input_coils_changed(modbus::device &dev, uint8_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::registers_changed(modbus::device &dev, int16_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::coils_changed(modbus::device &dev, uint8_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::input_registers_changed(modbus::device &dev, int16_t *buffer, int addr, int num);

%feature("nodirector") BusHandler::registers_write_error(modbus::device &dev, int16_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::coils_write_error(modbus::device &dev, uint8_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::registers_written(modbus::device &dev, int16_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::coils_written(modbus::device &dev, uint8_t *buffer, int addr, int num);

%feature("nodirector") BusHandler::input_coils_read_error(modbus::device &dev, uint8_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::registers_read_error(modbus::device &dev, int16_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::coils_read_error(modbus::device &dev, uint8_t *buffer, int addr, int num);
%feature("nodirector") BusHandler::input_registers_read_error(modbus::device &dev, int16_t *buffer, int addr, int num);

%feature("nodirector") BusHandler::log;

%include "bus.h"

%include "pybushandler.h"

