/***************************************************
 * pybushandler.h
 * Created on Mon, 16 Sep 2019 13:22:23 +0000 by vladimir
 *
 * $Author$
 * $Rev$
 * $Date$
 ***************************************************/
#pragma once
#include <iostream>
#include "bus.h"

class BusHandler_Base : public modbus::bus_handler {
public:
  virtual ~BusHandler_Base() { }
  virtual void device_added(modbus::device &dev) = 0;
  virtual void connection_error() = 0;

  virtual void input_coils_changed(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) = 0;
  virtual void registers_changed(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) = 0;
  virtual void coils_changed(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) = 0;
  virtual void input_registers_changed(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) = 0;

  virtual void registers_write_error(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) = 0;
  virtual void coils_write_error(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) = 0;
  virtual void registers_written(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) = 0;
  virtual void coils_written(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) = 0;

  virtual void input_coils_read_error(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) = 0;
  virtual void registers_read_error(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) = 0;
  virtual void coils_read_error(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) = 0;
  virtual void input_registers_read_error(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) = 0;

private:
  void input_coils_changed(modbus::device &dev, uint8_t *buffer, int addr, int num) {
    this->input_coils_changed(dev, std::vector<bool>(buffer, buffer + addr + num), addr, num);
  }
  void registers_changed(modbus::device &dev, int16_t *buffer, int addr, int num) {
    this->registers_changed(dev, std::vector<int16_t>(buffer, buffer + addr + num), addr, num);
  }
  void coils_changed(modbus::device &dev, uint8_t *buffer, int addr, int num) {
    this->coils_changed(dev, std::vector<bool>(buffer, buffer + addr + num), addr, num);
  }
  void input_registers_changed(modbus::device &dev, int16_t *buffer, int addr, int num) {
    this->input_registers_changed(dev, std::vector<int16_t>(buffer, buffer + addr + num), addr, num);
  }

  void registers_write_error(modbus::device &dev, int16_t *buffer, int addr, int num) {
    this->registers_write_error(dev, std::vector<int16_t>(buffer, buffer + addr + num), addr, num);
  }
  void coils_write_error(modbus::device &dev, uint8_t *buffer, int addr, int num) {
    this->coils_write_error(dev, std::vector<bool>(buffer, buffer + addr + num), addr, num);
  }
  void registers_written(modbus::device &dev, int16_t *buffer, int addr, int num) {
    this->registers_written(dev, std::vector<int16_t>(buffer, buffer + addr + num), addr, num);
  }
  void coils_written(modbus::device &dev, uint8_t *buffer, int addr, int num) {
    this->coils_written(dev, std::vector<bool>(buffer, buffer + addr + num), addr, num);
  }

  void input_coils_read_error(modbus::device &dev, uint8_t *buffer, int addr, int num) {
    this->input_coils_read_error(dev, std::vector<bool>(buffer, buffer + addr + num), addr, num);
  }
  void registers_read_error(modbus::device &dev, int16_t *buffer, int addr, int num) {
    this->registers_read_error(dev, std::vector<int16_t>(buffer, buffer + addr + num), addr, num);
  }
  void coils_read_error(modbus::device &dev, uint8_t *buffer, int addr, int num) {
    this->coils_read_error(dev, std::vector<bool>(buffer, buffer + addr + num), addr, num);
  }
  void input_registers_read_error(modbus::device &dev, int16_t *buffer, int addr, int num) {
    this->input_registers_read_error(dev, std::vector<int16_t>(buffer, buffer + addr + num), addr, num);
  }
};

class BusHandler : public BusHandler_Base {
public:
  virtual ~BusHandler() { }
  virtual void device_added(modbus::device &dev) {
  }

  virtual void input_coils_changed(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) {
  }
  virtual void registers_changed(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) {
  }
  virtual void coils_changed(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) {
  }
  virtual void input_registers_changed(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) {
  }

  virtual void registers_write_error(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) {
  }
  virtual void coils_write_error(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) {
  }
  virtual void registers_written(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) {
  }
  virtual void coils_written(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) {
  }

  virtual void input_coils_read_error(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) {
  }
  virtual void registers_read_error(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) {
  }
  virtual void coils_read_error(modbus::device &dev, std::vector<bool> const &buffer, int addr, int num) {
  }
  virtual void input_registers_read_error(modbus::device &dev, std::vector<int16_t> const &buffer, int addr, int num) {
  }
  virtual void connection_error() {
  }
};

