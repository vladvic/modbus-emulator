What is this
------------

This is a modbus slave emulator, supporting modbus TCP, RTU and ASCII.

Building
--------

You will need cmake (https://cmake.org/) and C++ compiler for your operating system.

To build the project do the following:

```bash
$ mkdir build && cd build
$ cmake ../ && make
```

if you want to build a Python module, you will also need SWIG (http://www.swig.org/) and Python development libraries.

If you have more than one Python version you can specify the one you want to build for by using PYTHON_VERSION variable:
```bash
$ cmake -DPYTHON_VERSION=2.7 ..
```

Usage
-----

./modbus-emu -c config-file.conf


Configuration
-------------

Configuration file is written in json format. Sample configuration is provided below:
```json
{
  "type":"tcp-listen",
  "address":"0.0.0.0:5502",
  "devices":[
    {
      "slave-id":10,
      "coils": {"1": false, "2": false, "3": true},
      "input-coils": {"1": false, "2": false, "3": false},
      "input-registers": {"1": 10, "2": 121, "3": 400},
      "registers": {"1": 10, "2": 10, "3": 10}
    }
  ]
}
```

Possible values for type: tcp, tcp-listen, rtu, ascii.
Address configuration:
  * for TCP: ```<source-ip>:<port>```
  * for TCP_LISTEN: ```<bind-ip>:<port>```
  * for RTU and ASCII: ```<path-to-device>:<speed>,<parity:N/O/E>,<bits>,<stop-bits>[,<RS232/RS485>]```


Python module
-------------

There is a Python module provided by SWIG generated python interface.
How to use the python module:
  * Import pymodbus:
      ```python
      from pymodbus import BusHandler, slave
      ```

  * Create a modbus event handler inherited from BusHandler:
      ```python
      class PythonBusHandler(BusHandler):
          def __init__(self):
              super().__init__()
      ...
      ```

  * Override event handling functions in BusHandler:
      ```python
          def registers_written(self, dev, buffer, addr, num):
            # addr - starting address, num - number of registers
            ...
          def coils_written(self, dev, buffer, addr, num):
            # addr - starting address, num - number of coils
            ...
      ```

  * Create bus object and assign the handler:
      ```python
      bus = slave("tcp-listen", "0.0.0.0:8000")
      bus.set_bus_handler(PythonBusHandler())
      ```

  * Add modbus device IDs and initialize registers and coils:
      ```python
      bus.add_device(1)
      bus.get_device(1).set_output_register(0, 0x0102)
      bus.get_device(1).set_output_register(1, 0x0101)
      bus.get_device(1).set_input_register(4, 0xf20)
      bus.get_device(1).set_input_coil(4, 1)
      ```

  * Set max coil/register address if necessary:
      ```python
      bus.set_max_registers(22)
      ```

  * Start bus processing:
      ```python
      while True:
          bus.process(100)
      ```

Available event handlers for modbus slave:

  * ```BusHandler::device_added(dev: modbus.device)```
  * ```BusHandler::connection_error()```
  * ```BusHandler::registers_written(dev: modbus.device, buffer: List(int), addr: int, num: int)```
  * ```BusHandler::coils_written(dev: modbus.device, buffer: List(bool), addr: int, num: int)```


Credits
-------

This software uses following 3rd party libraries (bundled with the source code):

  * pjson by Rich Geldreich (License: Unlicense http://unlicense.org/) - fastest JSON parser ever

  * libmodbus by Stéphane Raimbault &lt;stephane.raimbault@gmail.com&gt;, Tobias Doerffel &lt;tobias.doerffel@gmail.com&gt;, 
    Florian Forster &lt;ff@octo.it&gt;, oldfaber &lt;oldfaber@gmail.com&gt; (License: LGPL) - modbus access library

